export type Pagination = {
  offset: number,
  perPage: number,
  pageCount: number,
  currentPage: number,
};
