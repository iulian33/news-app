// @flow
import axios from "axios";

export default class News {
  request = (endPoint: string) =>
    new Promise<void>((resolve) => {
      axios.get(endPoint).then((res) => {
        resolve(res.data);
      });
    });

  getNews = () => {
    const endPoint = `https://finnhub.io/api/v1/company-news?symbol=AAPL&from=2020-01-01&token=btvcra748v6o8d960as0`;
    return this.request(endPoint);
  };
}
