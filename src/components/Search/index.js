// @flow
import React, { useEffect, useRef } from "react";
import { connect } from "react-redux";
import { SearchContainer, SearchIcon } from "components/Search/style";
import { updateSearchAction } from "redux/modules/search";
import { resetAllNewsAction } from "redux/modules/news";
import { resetAllBookmarksAction } from "redux/modules/bookmarks";

type Props = {
  navigationType: string,
  dispatch: ({ type: string }) => void,
};

let searchTimeout = 0;

const Search = ({ dispatch, navigationType }: Props) => {
  const usePrevious = (value) => {
    const ref = useRef();
    useEffect(() => {
      ref.current = value;
    });
    return ref.current;
  };

  const prevNavigation = usePrevious(navigationType);
  let inputValue: HTMLInputElement;

  useEffect(() => {
    if (prevNavigation !== navigationType) {
      inputValue.value = "";
      navigationType === "news" ? dispatch(resetAllNewsAction()) : dispatch(resetAllBookmarksAction());
    }
  });

  const onSearch = (event) => {
    const searchValue = event.target.value;

    if (searchTimeout) {
      clearTimeout(searchTimeout);
    }

    searchTimeout = setTimeout(() => {
      dispatch(
        updateSearchAction({
          searchText: searchValue,
        })
      );
    }, 1000);
  };
  return (
    <SearchContainer>
      <SearchIcon className="icon-search" />
      <input type="text" ref={(el: any) => (inputValue = el)} placeholder="Search" onKeyUp={onSearch} />
    </SearchContainer>
  );
};

const mapStateToProps = ({ search, app }) => ({
  searchText: search.searchText,
  navigationType: app.navigationType,
});

export default connect(mapStateToProps)(Search);
