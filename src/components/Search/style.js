import styled from "styled-components";

export const SearchContainer = styled.div`
  background: #191818;
  overflow: hidden;
  position: relative;
  border-radius: 5px;
  margin-top: -5px;
  input {
    font-family: "Ubuntu", sans-serif;
    background: transparent;
    outline: 0;
    width: 100%;
    border: none;
    padding: 9px 25px 12px 35px;
    font-size: 12px;
    color: #ccc;
  }
`;

export const SearchIcon = styled.i`
  color: #fff;
  position: absolute;
  left: 10px;
  top: 10px;
  font-size: 16px;
`;
