// @flow
import React from "react";
import { createGlobalStyle } from "styled-components";
import reset from "styled-reset";

type Props = {
  children: any,
};

const GlobalStyle = createGlobalStyle`
    ${reset};
    @import url('https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;700&display=swap');
    html { 
        box-sizing: border-box; 
    }      

    body {
        background: #242525;
        position: relative;
        font-family: 'Ubuntu', sans-serif; 
    } 
    
    h1,h2,h3,h4,h5,h6 {
      font-weight: 700;
    }

    button{ 
        padding: 0;
        border-width: 0;
        border-radius: 0;
        font-family:inherit;
        background: none;
        &:focus {
            outline: none;
        }
    }
    
    #root{ 
        height: 100%;
    }
    *, ::after, ::before { 
        box-sizing: border-box; 
		    -webkit-tap-highlight-color: rgba(0,0,0,0);
    }
    .noselect {
        user-select: none;
    }
`;
const LayoutContainer = (props: Props) => (
  <>
    <GlobalStyle />
    {props.children}
  </>
);

export default LayoutContainer;
