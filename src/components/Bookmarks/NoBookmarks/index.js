// @flow
import React from "react";
import styled from "styled-components";

const Message = styled.h2`
  font-size: 45px;
  position: absolute;
  top: 45%;
  left: 55%;
  color: #fff;
`;

const NoBookmarks = () => {
  return <Message>No Bookmarks !</Message>;
};

export default NoBookmarks;
