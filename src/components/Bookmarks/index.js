// @flow
import React from "react";
import { connect } from "react-redux";
import { Row } from "react-flexbox-grid";
import type { Bookmark } from "models/Bookmark";
import NoBookmarks from "components/Bookmarks/NoBookmarks";
import { PostCol } from "components/News/style";
import Post from "components/Post";
import Pagination from "components/Pagination";

type Props = {
  bookmarks: Bookmark[],
  allBookmarks: Bookmark[],
};

const Bookmarks = ({ bookmarks, allBookmarks }: Props) => {
  const loadAllBookmarks = () => {
    return bookmarks.map((post: Bookmark, index: number) => {
      return (
        <PostCol sm={4} key={index}>
          <Post post={post} />
        </PostCol>
      );
    });
  };

  return (
    <>
      {!!allBookmarks.length ? (
        <>
          <Row>{loadAllBookmarks()}</Row>
          <Pagination postsLength={allBookmarks.length} />
        </>
      ) : (
        <NoBookmarks />
      )}
    </>
  );
};
const mapStateToProps = ({ bookmarks }) => ({
  bookmarks: bookmarks.paginatedBookmarks,
  allBookmarks: bookmarks.allBookmarks,
});

export default connect(mapStateToProps)(Bookmarks);
