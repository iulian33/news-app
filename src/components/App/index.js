// @flow
import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import defaultHOCs from "hocs/defaultHOCs";
import LayoutContainer from "components/Layout";
import Header from "components/Header";
import Routes from "components/Routes";
import Body from "components/Body";
import "assets/icomoon/style.css";

function App() {
  return (
    <LayoutContainer>
      <Router>
        <Header />
        <Body>
          <Routes />
        </Body>
      </Router>
    </LayoutContainer>
  );
}

export default defaultHOCs(App);
