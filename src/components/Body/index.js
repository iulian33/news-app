// @flow
import React from "react";
import { BodyContainer, LatestPost, MainLoad, Structure } from "components/Body/style";
import LatestNew from "components/News/LatestNew";

type Props = {
  children: any,
};

const Body = (props: Props) => {
  return (
    <BodyContainer>
      <Structure>
        <LatestPost sm={4}>
          <LatestNew />
        </LatestPost>
        <MainLoad sm={8}>{props.children}</MainLoad>
      </Structure>
    </BodyContainer>
  );
};

export default Body;
