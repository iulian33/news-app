import styled from "styled-components";
import { Col, Row } from "react-flexbox-grid";

export const BodyContainer = styled.div`
  padding: 0 50px 100px;
`;

export const Structure = styled(Row)``;

export const LatestPost = styled(Col)`
  padding-right: 16px;
`;

export const MainLoad = styled(Col)``;
