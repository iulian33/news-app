// @flow
import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import News from "components/News";
import Bookmarks from "components/Bookmarks";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Redirect to="/news" />
      </Route>
      <Route path="/news">
        <News />
      </Route>
      <Route path="/bookmarks">
        <Bookmarks />
      </Route>
    </Switch>
  );
};

export default Routes;
