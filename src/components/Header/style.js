import styled from "styled-components";

export const Menu = styled.ul`
  padding: 35px 50px;
  font-size: 28px;
`;

export const Item = styled.li`
  display: inline-block;
  padding: 0 15px;
  font-weight: 700;
  a {
    text-decoration: none;
    color: #fff;
    opacity: 0.6;
    &.active {
      opacity: 1;
    }
  }
  &:first-child {
    padding-left: 0;
  }
`;

export const SearchItem = styled.li`
  float: right;
  width: 100%;
  max-width: 300px;
`;
