// @flow
import React from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import { Item, Menu, SearchItem } from "components/Header/style";
import Search from "components/Search";
import { changNavigationTypeAction } from "redux/modules/app";

type Props = {
  dispatch: ({ type: string }) => void,
};

const Header = (props: Props) => {
  return (
    <Menu>
      <Item>
        <NavLink
          to="/news"
          activeClassName="active"
          onClick={() => {
            props.dispatch(changNavigationTypeAction("news"));
          }}
        >
          News
        </NavLink>
      </Item>
      <Item>
        <NavLink
          to="/bookmarks"
          activeClassName="active"
          onClick={() => {
            props.dispatch(changNavigationTypeAction("bookmarks"));
          }}
        >
          Bookmarks
        </NavLink>
      </Item>
      <SearchItem>
        <Search />
      </SearchItem>
    </Menu>
  );
};

export default connect()(Header);
