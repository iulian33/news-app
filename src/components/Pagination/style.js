import styled from "styled-components";
import { Row } from "react-flexbox-grid";

export const PaginationContainer = styled(Row)`
  padding-top: 12px;

  .pagination {
    clear: both;
    float: right;
    display: inline-flex;
    flex-direction: row;
    margin-top: -5px;
    .active {
      display: none;
    }

    > li {
      cursor: pointer;
      margin-left: 15px;

      a {
        display: block;
        background: #3c3c3c;
        border-radius: 50px;
        padding: 10px 50px;
        font-size: 12px;
        -moz-user-select: none;
        outline: 0;
        font-weight: 700;
        -webkit-user-select: none;
        user-select: none;
        color: #fff;
        transition: all 0.3s ease;
        &:hover {
          background: #2e2e2e;
        }
      }
      &.disabled {
        opacity: 0.2;
        a {
          &:hover {
            background: #3c3c3c;
          }
        }
      }
    }
  }
`;

export const Indicators = styled.div`
  color: #fff;
  font-size: 14px;
  > span {
    padding-left: 7px;
    opacity: 0.25;
  }
`;
