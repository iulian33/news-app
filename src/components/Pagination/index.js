// @flow
import React from "react";
import { Col } from "react-flexbox-grid";
import ReactPaginate from "react-paginate";
import { updatePaginationAction } from "redux/modules/pagination";
import { Indicators, PaginationContainer } from "components/Pagination/style";
import { connect } from "react-redux";

type Props = {
  offset: number,
  pageCount: number,
  perPage: number,
  postsLength: number,
  currentPage: number,
  dispatch: ({ type: string }) => void,
};

const Pagination = (props: Props) => {
  const handlePageClick = (e) => {
    const selectedPage = e.selected;
    const offset = selectedPage * props.perPage;
    props.dispatch(updatePaginationAction({ currentPage: selectedPage, offset: offset }));
  };

  const getIndicators = () => {
    const offset = props.offset + 1;
    const range = offset === props.postsLength ? `${props.postsLength}` : `${offset} - ${offset + props.perPage - 1}`;
    return (
      <>
        {range} <span> out of {props.postsLength}</span>
      </>
    );
  };
  return (
    <PaginationContainer>
      <Col sm={6}>
        <Indicators>{getIndicators()}</Indicators>
      </Col>
      <Col sm={6}>
        <ReactPaginate
          previousLabel={"Previews"}
          nextLabel={"Next"}
          breakLabel={""}
          breakClassName={"break-me"}
          pageCount={props.pageCount}
          forcePage={props.currentPage}
          marginPagesDisplayed={0}
          pageRangeDisplayed={0}
          onPageChange={handlePageClick}
          containerClassName={"pagination"}
          subContainerClassName={"pages pagination"}
          activeClassName={"active"}
        />
      </Col>
    </PaginationContainer>
  );
};

const mapStateToProps = ({ pagination }) => ({
  offset: pagination.offset,
  pageCount: pagination.pageCount,
  perPage: pagination.perPage,
  currentPage: pagination.currentPage,
});

export default connect(mapStateToProps)(Pagination);
