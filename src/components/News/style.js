import styled from "styled-components";
import { Col } from "react-flexbox-grid";

export const PostCol = styled(Col)`
  padding-bottom: 22px;
`;
