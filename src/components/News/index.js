// @flow
import React from "react";
import { connect } from "react-redux";
import type { New } from "models/New";
import Post from "components/Post";
import { PostCol } from "components/News/style";
import { Row } from "react-flexbox-grid";
import Pagination from "components/Pagination";
import NoNews from "components/News/NoNews";

type Props = {
  news: New[],
  allNews: New[],
};

const News = ({ news, allNews }: Props) => {
  const loadAllNews = () => {
    return news.map((post: New, index: number) => {
      return (
        <PostCol sm={4} key={index}>
          <Post post={post} />
        </PostCol>
      );
    });
  };

  return !!news.length ? (
    <>
      <Row>{loadAllNews()}</Row>
      <Pagination postsLength={allNews.length} />
    </>
  ) : (
    <NoNews />
  );
};

const mapStateToProps = ({ news }) => ({
  news: news.paginatedNews,
  allNews: news.allNews,
});

export default connect(mapStateToProps)(News);
