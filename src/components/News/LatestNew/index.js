// @flow
import React from "react";
import Post from "components/Post";
import { connect } from "react-redux";
import type { New } from "models/New";

type Props = {
  latestNew: New,
};

const LatestNew = (props: Props) => {
  return !!Object.keys(props.latestNew).length && <Post post={props.latestNew} status="latest" />;
};

const mapStateToProps = ({ news }) => ({
  latestNew: news.latestNew,
});

export default connect(mapStateToProps)(LatestNew);
