// @flow
import React from "react";
import { connect } from "react-redux";
import { Col, Row } from "react-flexbox-grid";
import type { New } from "models/New";
import type { Bookmark } from "models/Bookmark";
import { addBookmarkAction, removeBookmarkAction } from "redux/modules/bookmarks";
import { ActionButtons, BottomInfo, Category, LatestLabel, PostContainer, PostContent } from "components/Post/style";

type Props = {
  status?: string,
  post: New,
  savedBookmarks: Bookmark[],
  dispatch: ({ type: string }) => void,
};

const Post = (props: Props) => {
  const formatDate = (timestamp: number) => {
    const date = new Date(timestamp * 1000);
    const month = date.toLocaleString("default", { month: "short" });
    return `${date.getDate()} ${month}`;
  };

  const calculateReadTime = (text) => {
    const wordsPerMinute = 130;
    const noOfWords = text.split(/\s/g).length;
    const minutes = noOfWords / wordsPerMinute;
    const readTime = Math.ceil(minutes);
    return `${readTime} min read`;
  };

  const isPostInBookmark = () => {
    const postChecked = props.savedBookmarks.filter((bookmark) => {
      return bookmark && bookmark.id === props.post.id;
    });
    return postChecked.length;
  };

  return (
    <PostContainer status={props.status} image={props.post.image}>
      <PostContent status={props.status}>
        <Row>
          <Col sm={6}>
            <Category>{props.post.related}</Category>
          </Col>
          <Col sm={6}>{props.status === "latest" && <LatestLabel>Latest research</LatestLabel>}</Col>
        </Row>
        <BottomInfo status={props.status}>
          <h2>{props.post.headline}</h2>
          <Row>
            <Col xs={props.status === "latest" ? 10 : 8}>
              <ul className="meta-data">
                {props.status === "latest" && (
                  <li>
                    <a href={props.post.url} target="_blank" rel="noopener noreferrer">
                      <i className="icon-arrow-right-circle" />
                      <span>Read the research</span>
                    </a>
                  </li>
                )}
                <li>{formatDate(props.post.datetime)}</li>
                <li>{calculateReadTime(props.post.summary)}</li>
              </ul>
            </Col>
            <ActionButtons xs={props.status === "latest" ? 2 : 4}>
              <a href={props.post.url} target="_blank" rel="noopener noreferrer">
                <i className="icon-forward" />
              </a>
              {isPostInBookmark() ? (
                <i
                  className="icon-bookmark-add"
                  onClick={() => {
                    props.dispatch(removeBookmarkAction(props.post.id));
                  }}
                />
              ) : (
                <i
                  className="icon-bookmark-delete"
                  onClick={() => {
                    props.dispatch(addBookmarkAction(props.post));
                  }}
                />
              )}
            </ActionButtons>
          </Row>
        </BottomInfo>
      </PostContent>
    </PostContainer>
  );
};

const mapStateToProps = ({ bookmarks }) => ({
  savedBookmarks: bookmarks.savedBookmarks,
});

export default connect(mapStateToProps)(Post);
