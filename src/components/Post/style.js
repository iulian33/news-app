import styled from "styled-components";
import { Col } from "react-flexbox-grid";

export const PostContainer = styled.div`
  position: relative;
  display: inline-block;
  border-radius: 5px;
  overflow: hidden;
  width: 100%;
  background-image: ${({ image }) => image && `url("${image}")`};
  background-size: cover;
  background-position: center center;
  box-shadow: 4px 6px 20px 0 rgba(0, 0, 0, 0.5);
  min-height: ${({ status }) => (status === "latest" ? "40vw" : "28vw")};
`;

export const PostContent = styled.div`
  background-color: rgba(6, 7, 8, 0.6);
  width: 100%;
  height: 100%;
  padding: ${({ status }) => (status === "latest" ? "30px 25px 35px" : "30px")};
  position: absolute;
`;

export const Category = styled.div`
  color: #fff;
  font-size: 10px;
  padding: 5px 15px 4px;
  display: inline-block;
  border-radius: 50px;
  border: 1px solid #fff;
`;

export const LatestLabel = styled.span`
  background: #931636;
  padding: 7px 15px;
  color: #fff;
  border-radius: 3px;
  font-size: 10px;
  float: right;
`;

export const BottomInfo = styled.div`
  position: absolute;
  bottom: 30px;
  width: calc(100% - 50px);
  color: #fff;
  .meta-data {
    font-size: 12px;
    li {
      display: inline-block;
      padding-right: 15px;
      border-right: 1px solid rgba(255, 255, 255, 0.1);
      margin-right: 15px;
      a {
        color: #fff;
        text-decoration: none;
      }

      &:nth-child(1) {
        font-weight: 700;
        position: relative;
        i {
          position: absolute;
          top: -5px;
          left: 0;
          font-size: 20px;
        }
        span {
          padding-left: 30px;
        }
      }
      &:nth-child(2) {
        opacity: 0.7;
        font-size: 13px;
      }

      &:nth-child(3) {
        opacity: 0.3;
      }
      &:last-child {
        border-right: none;
        margin-right: 0;
        padding-right: 0;
      }
    }
  }
  h2 {
    font-size: ${({ status }) => (status === "latest" ? "24px" : "20px")};
    padding-bottom: ${({ status }) => (status === "latest" ? "65px" : "35px")};
    line-height: 30px;
    font-weight: 400;
    padding-right: 15px;
  }
`;

export const ActionButtons = styled(Col)`
  text-align: right;
  a {
    color: #fff;
    text-decoration: none;
  }
  i {
    padding-left: 15px;
    cursor: pointer;
  }
`;
