import { combineReducers } from "redux";
import app from "./modules/app";
import news from "./modules/news";
import bookmarks from "./modules/bookmarks";
import pagination from "./modules/pagination";
import search from "./modules/search";

export default combineReducers({
  app,
  news,
  bookmarks,
  search,
  pagination,
});
