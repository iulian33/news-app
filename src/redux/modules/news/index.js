// @flow
import type { New } from "models/New";
import type { Pagination } from "models/Pagination";

const GET_NEWS = "GET_NEWS";
const SET_NEWS = "SET_NEWS";
const RESET_NEWS = "RESET_NEWS";
const SET_PAGINATED_NEWS = "SET_PAGINATED_NEWS";
const SET_LATEST_NEW = "SET_LATEST_NEW";
const SAVE_PAGING_CONFIG = "SAVE_PAGING_CONFIG";

export type Action =
  | { type: typeof GET_NEWS }
  | { type: typeof SET_NEWS, allNews: New[] }
  | { type: typeof RESET_NEWS }
  | { type: typeof SET_PAGINATED_NEWS, paginatedNews: New[] }
  | { type: typeof SET_LATEST_NEW, latestNew: New }
  | { type: typeof SAVE_PAGING_CONFIG, pagingConfig: Pagination };

export type State = {
  allNews: New[],
  paginatedNews: New[],
  latestNew: New,
  pagingConfig: Pagination,
};

const initialState: State = {
  allNews: [],
  paginatedNews: [],
  latestNew: {},
  pagingConfig: {},
};

export default function reducer(state: State = initialState, action: Action): State {
  switch (action.type) {
    case SET_NEWS: {
      return {
        ...state,
        allNews: action.allNews,
      };
    }
    case SET_PAGINATED_NEWS: {
      return {
        ...state,
        paginatedNews: action.paginatedNews,
      };
    }
    case SET_LATEST_NEW: {
      return {
        ...state,
        latestNew: action.latestNew,
      };
    }
    case SAVE_PAGING_CONFIG: {
      return {
        ...state,
        pagingConfig: action.pagingConfig,
      };
    }
    default: {
      return state;
    }
  }
}

export const getNewsAction = (): Action => ({
  type: GET_NEWS,
});

export const setAllNewsAction = (allNews: New[]): Action => ({
  type: SET_NEWS,
  allNews,
});

export const resetAllNewsAction = (): Action => ({
  type: RESET_NEWS,
});

export const setPaginatedNewsAction = (paginatedNews: New[]): Action => ({
  type: SET_PAGINATED_NEWS,
  paginatedNews,
});

export const setLatestNewAction = (latestNew: New): Action => ({
  type: SET_LATEST_NEW,
  latestNew,
});

export const savePagingConfigAction = (pagingConfig: Pagination): Action => ({
  type: SAVE_PAGING_CONFIG,
  pagingConfig,
});
