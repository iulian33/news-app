import { NewsService } from "services";
import { savePagingConfigAction, setAllNewsAction, setLatestNewAction } from "redux/modules/news/index";
import { updatePaginationAction } from "redux/modules/pagination";

const newsMiddleware = (store) => (next) => (action) => {
  switch (action.type) {
    case "GET_NEWS": {
      NewsService.getNews()
        .then((data) => {
          const { perPage } = store.getState().pagination;

          const timestampArray = data.map((post) => {
            return post.datetime;
          });
          const latestPost = data.filter((post) => post.datetime === Math.max(...timestampArray) && post);
          const filteredData = data.filter((post) => post.datetime !== Math.max(...timestampArray) && post);

          store.dispatch(setAllNewsAction(filteredData));
          store.dispatch(setLatestNewAction(...latestPost));
          store.dispatch(updatePaginationAction({ pageCount: Math.ceil(data.length / perPage) }));
        })
        .catch(() => {
          console.log("Stopped because of to many requests !");
        });
      break;
    }

    case "CHANGE_NAVIGATION_TYPE": {
      if (action.navigationType === "news") {
        const { pagingConfig } = store.getState().news;
        store.dispatch(updatePaginationAction(pagingConfig));
      }
      break;
    }
    case "SET_PAGINATED_NEWS": {
      const { pagination } = store.getState();
      store.dispatch(savePagingConfigAction(pagination));
      break;
    }
    default:
  }

  next(action);
};

export default newsMiddleware;
