import { setAllNewsAction } from "redux/modules/news";
import { updatePaginationAction } from "redux/modules/pagination";
import type { New } from "models/New";
import { setAllBookmarksAction } from "redux/modules/bookmarks";

let initialNews: New[] = [];

const searchMiddleware = (store) => (next) => (action) => {
  switch (action.type) {
    case "UPDATE_SEARCH": {
      const { app, pagination, news, bookmarks } = store.getState();
      if (!initialNews.length) initialNews = news.allNews.slice();

      if (app.navigationType === "news") {
        const filteredNews =
          action.payload.searchText !== ""
            ? initialNews.filter(
                (post: New) => post.headline.toUpperCase().includes(action.payload.searchText.toUpperCase()) && post
              )
            : initialNews;

        store.dispatch(
          updatePaginationAction({
            offset: 0,
            currentPage: 0,
            pageCount: Math.ceil(filteredNews.length / pagination.perPage),
          })
        );
        store.dispatch(setAllNewsAction(filteredNews));
      } else {
        const filteredBookmarks =
          action.payload.searchText !== ""
            ? bookmarks.savedBookmarks.filter(
                (post: New) => post.headline.toUpperCase().includes(action.payload.searchText.toUpperCase()) && post
              )
            : bookmarks.savedBookmarks;
        store.dispatch(
          updatePaginationAction({
            offset: 0,
            currentPage: 0,
            pageCount: Math.ceil(filteredBookmarks.length / pagination.perPage),
          })
        );
        store.dispatch(setAllBookmarksAction(filteredBookmarks));
      }
      break;
    }

    case "RESET_NEWS": {
      const { news } = store.getState();
      if (!initialNews.length) initialNews = news.allNews.slice();
      const { perPage } = store.getState().pagination;
      store.dispatch(setAllNewsAction(initialNews));
      store.dispatch(
        updatePaginationAction({
          offset: 0,
          currentPage: 0,
          pageCount: Math.ceil(initialNews.length / perPage),
        })
      );
      break;
    }

    case "RESET_BOOKMARKS": {
      const { bookmarks, pagination } = store.getState();

      store.dispatch(setAllBookmarksAction(bookmarks.savedBookmarks));
      store.dispatch(
        updatePaginationAction({
          offset: 0,
          currentPage: 0,
          pageCount: Math.ceil(bookmarks.savedBookmarks.length / pagination.perPage),
        })
      );
      break;
    }
    default:
  }

  next(action);
};

export default searchMiddleware;
