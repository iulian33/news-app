// @flow

const UPDATE_SEARCH = "UPDATE_SEARCH";

type Search = {
  searchText: string,
};

export type Action = { type: typeof UPDATE_SEARCH, payload: Search };

const initialState: Search = {
  searchText: "",
};

export default function reducer(state: Search = initialState, action: Action) {
  switch (action.type) {
    case UPDATE_SEARCH: {
      return {
        ...state,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
}

export const updateSearchAction = (payload: Search): Action => ({
  type: UPDATE_SEARCH,
  payload,
});
