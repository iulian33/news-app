import { saveBookmarkPagingConfigAction, setPaginatedBookmarksAction } from "redux/modules/bookmarks/index";
import { updatePaginationAction } from "redux/modules/pagination";

const bookmarkMiddleware = (store) => (next) => (action) => {
  switch (action.type) {
    case "ADD_BOOKMARK":
    case "REMOVE_BOOKMARK": {
      setTimeout(() => {
        const { bookmarks, pagination, app } = store.getState();

        if (app.navigationType === "bookmarks") {
          const paginatedBookmarks = bookmarks.savedBookmarks.slice(
            pagination.offset,
            pagination.offset + pagination.perPage
          );
          store.dispatch(setPaginatedBookmarksAction(paginatedBookmarks));
        }
      });
      break;
    }
    case "CHANGE_NAVIGATION_TYPE": {
      if (action.navigationType === "bookmarks") {
        const { bookmarks, pagination } = store.getState();
        bookmarks.pagingConfig.pageCount = Math.ceil(bookmarks.allBookmarks.length / pagination.perPage);
        store.dispatch(updatePaginationAction(bookmarks.pagingConfig));
      }
      break;
    }

    case "SET_PAGINATED_BOOKMARKS": {
      const { pagination } = store.getState();
      store.dispatch(saveBookmarkPagingConfigAction(pagination));
      break;
    }
    default:
  }

  next(action);
};

export default bookmarkMiddleware;
