// @flow
import type { Pagination } from "models/Pagination";
import type { Bookmark } from "models/Bookmark";

const ADD_BOOKMARK = "ADD_BOOKMARK";
const REMOVE_BOOKMARK = "REMOVE_BOOKMARK";
const SET_BOOKMARKS = "SET_BOOKMARKS";
const RESET_BOOKMARKS = "RESET_BOOKMARKS";
const SET_PAGINATED_BOOKMARKS = "SET_PAGINATED_BOOKMARKS";
const SAVE_BOOKMARK_PAGING_CONFIG = "SAVE_BOOKMARK_PAGING_CONFIG";

export type Action =
  | { type: typeof RESET_BOOKMARKS }
  | { type: typeof ADD_BOOKMARK, bookmark: Bookmark[] }
  | { type: typeof REMOVE_BOOKMARK, bookmarkId: number }
  | { type: typeof SET_BOOKMARKS, allBookmarks: Bookmark[] }
  | { type: typeof SET_PAGINATED_BOOKMARKS, paginatedBookmarks: Bookmark[] }
  | { type: typeof SAVE_BOOKMARK_PAGING_CONFIG, pagingConfig: Pagination };

export type State = {
  allBookmarks: Bookmark[],
  paginatedBookmarks: Bookmark[],
  savedBookmarks: Bookmark[],
  pagingConfig: Pagination,
};

const initialState: State = {
  allBookmarks: [],
  savedBookmarks: [],
  paginatedBookmarks: [],
  pagingConfig: {
    offset: 0,
    perPage: 6,
    pageCount: 1,
    currentPage: 0,
  },
};

export default function reducer(state: State = initialState, action: Action): State {
  switch (action.type) {
    case ADD_BOOKMARK: {
      return {
        ...state,
        savedBookmarks: [...state.savedBookmarks, action.bookmark],
      };
    }
    case REMOVE_BOOKMARK: {
      const bookmarks = state.savedBookmarks.filter((bookmark) => {
        return bookmark && bookmark.id !== action.bookmarkId;
      });
      return {
        ...state,
        savedBookmarks: bookmarks,
      };
    }
    case SET_BOOKMARKS: {
      return {
        ...state,
        allBookmarks: action.allBookmarks,
      };
    }

    case SET_PAGINATED_BOOKMARKS: {
      return {
        ...state,
        paginatedBookmarks: action.paginatedBookmarks,
      };
    }
    case SAVE_BOOKMARK_PAGING_CONFIG: {
      return {
        ...state,
        pagingConfig: action.pagingConfig,
      };
    }
    default: {
      return state;
    }
  }
}

export const addBookmarkAction = (bookmark: Bookmark[]): Action => ({
  type: ADD_BOOKMARK,
  bookmark,
});

export const removeBookmarkAction = (bookmarkId: number): Action => ({
  type: REMOVE_BOOKMARK,
  bookmarkId,
});

export const setAllBookmarksAction = (allBookmarks: Bookmark[]): Action => ({
  type: SET_BOOKMARKS,
  allBookmarks,
});

export const resetAllBookmarksAction = (): Action => ({
  type: RESET_BOOKMARKS,
});

export const setPaginatedBookmarksAction = (paginatedBookmarks: Bookmark[]): Action => ({
  type: SET_PAGINATED_BOOKMARKS,
  paginatedBookmarks,
});

export const saveBookmarkPagingConfigAction = (pagingConfig: Pagination): Action => ({
  type: SAVE_BOOKMARK_PAGING_CONFIG,
  pagingConfig,
});
