// @flow
const CHANGE_NAVIGATION_TYPE = "CHANGE_NAVIGATION_TYPE";

export type Action = { type: typeof CHANGE_NAVIGATION_TYPE, navigationType: string };

export type State = {
  navigationType: string,
};

const initialState: State = {
  navigationType: "news",
};

export default function reducer(state: State = initialState, action: Action): State {
  switch (action.type) {
    case CHANGE_NAVIGATION_TYPE: {
      return {
        ...state,
        navigationType: action.navigationType,
      };
    }
    default: {
      return state;
    }
  }
}

export const changNavigationTypeAction = (navigationType: string): Action => ({
  type: CHANGE_NAVIGATION_TYPE,
  navigationType,
});
