// @flow
import type { Pagination } from "models/Pagination";

const UPDATE_PAGINATION = "UPDATE_PAGINATION";
const RESET_PAGINATION = "RESET_PAGINATION";

export type Action =
  | { type: typeof UPDATE_PAGINATION, payload: Pagination }
  | { type: typeof RESET_PAGINATION, currentPage: number };

const initialState: Pagination = {
  offset: 0,
  perPage: 6,
  pageCount: 1,
  currentPage: 0,
};

export default function reducer(state: Pagination = initialState, action: Action): Pagination {
  switch (action.type) {
    case UPDATE_PAGINATION: {
      return {
        ...state,
        ...action.payload,
      };
    }

    case RESET_PAGINATION: {
      return {
        ...state,
        currentPage: action.currentPage,
      };
    }
    default: {
      return state;
    }
  }
}

export const updatePaginationAction = (payload: Pagination): Action => ({
  type: UPDATE_PAGINATION,
  payload,
});

export const resetPaginationAction = (currentPage: number): Action => ({
  type: RESET_PAGINATION,
  currentPage,
});
