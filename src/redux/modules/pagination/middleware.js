import { setPaginatedNewsAction } from "redux/modules/news";
import { setPaginatedBookmarksAction } from "redux/modules/bookmarks";

const paginationMiddleware = (store) => (next) => (action) => {
  switch (action.type) {
    case "UPDATE_PAGINATION": {
      setTimeout(() => {
        const { news, bookmarks, pagination, app } = store.getState();
        if (app.navigationType === "news") {
          const paginatedNews = news.allNews.slice(pagination.offset, pagination.offset + pagination.perPage);
          store.dispatch(setPaginatedNewsAction(paginatedNews));
        } else {
          const paginatedBookmarks = bookmarks.allBookmarks.slice(
            pagination.offset,
            pagination.offset + pagination.perPage
          );
          store.dispatch(setPaginatedBookmarksAction(paginatedBookmarks));
        }
      });
      break;
    }
    default:
  }

  next(action);
};

export default paginationMiddleware;
