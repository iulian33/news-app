// @flow
import { applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import newsMiddleware from "./modules/news/middleware";
import paginationMiddleware from "./modules/pagination/middleware";
import bookmarkMiddleware from "./modules/bookmarks/middleware";
import searchMiddleware from "redux/modules/search/middleware";

const middleware = [thunk, newsMiddleware, bookmarkMiddleware, paginationMiddleware, searchMiddleware];

export default composeWithDevTools(applyMiddleware(...middleware));
