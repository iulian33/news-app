// @flow
import React from "react";
import { connect } from "react-redux";
import { getNewsAction } from "redux/modules/news";

type Props = {
  dispatch: ({ type: string }) => void,
};

const defaultHOCs = (ComposedComponent: Function) => {
  const component = (props: Props) => {
    props.dispatch(getNewsAction());
    return <ComposedComponent {...props} />;
  };

  return connect()(component);
};

export default defaultHOCs;
